# vcfR Workflow gf2


This geneflow2 workflow takes sorted BAM file and a FASTA reference file and performs variant calling on them. 

Inputs
------
1. BAM file
2. FASTA reference file

Outputs
-------
2. vcfR output directory including vcf file

Apps Documentation
------------------
Documentation for the individual apps are in their respective directories:

| https://gitlab.com/geneflow/apps/vcfr-gf2.git
